import { Outlet, Link } from "react-router-dom";
import React from "react";

class Layout extends React.Component {
  checkLogged = () => {
    const navigate = localStorage["logged"] === "true";
    console.log(true)
    if (navigate) {
      console.log("kkk")
      return <Link to="/profile">Profile</Link>
    }
    else {
      console.log("kkkffff")
      return <Link to="/login">Login</Link>
    }
  }
  render() {
    return (
    <>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/news">News</Link>
          </li>
          <li>
            <Link to={this.props.path}>{this.props.title}</Link>
            
          </li>
        </ul>
      </nav>

      <Outlet />
    </>
  )
}
};

export default Layout;