import React from "react";
import "../App.css"
import News from "../pages/News"
import Profile from "../pages/Profile"
import Layout from "../pages/Layout"
import Main from "../pages/Main"
import Login from "../pages/Login"
import { BrowserRouter, Routes, Route } from "react-router-dom";


class Router extends React.Component {
    constructor(props) {
        super(props);
        if (localStorage["logged"]) {
            this.state = {
                title: "Profile",
                path: "/profile"
            }
        }
        else {
            this.state = {
                title: "Login",
                path: "/login"
            }
        }
    }
    handleChange = (value1, value2) => {
        this.setState({ title: value1 });
        this.setState({ path: value2 });
    }
    render() {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout title={this.state.title} path={this.state.path} />}>
                        <Route index element={<Main />} />
                        <Route path="news" element={<News />} />
                        <Route path="profile" element={<Profile />} />
                        <Route path="login" element={<Login updateData={this.handleChange} />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        )
    }
};

export default Router;