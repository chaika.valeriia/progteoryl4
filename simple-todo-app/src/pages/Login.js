import React from "react";
import {useNavigate} from 'react-router-dom';


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }
    handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    }
    handleSubmit = (event) => {
        event.preventDefault();
        if (this.state.username === "Admin" && this.state.password === "12345") {
            localStorage.setItem("logged", true)
            this.props.updateData("Profile", "/profile");
            window.location.href = '/profile';
        }
        else {
            localStorage.setItem("logged", false)
            this.props.updateData("Login", "/login");
            alert("Incorrect username os password");
        }
    }
    render() {
        return (
            <><h3>Login</h3>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="username">
                            Username
                        </label>
                        <input id="username" type="text" name="username"
                            value={this.state.username || ""}
                            onChange={this.handleChange} />
                    </div>

                    <div>
                        <label htmlFor="password">
                            Password
                        </label>
                        <input id="password" type="text" name="password"
                            value={this.state.password || ""}
                            onChange={this.handleChange} />
                    </div>
                    <button className="button" type="submit">Login</button>
                </form></>
        )
    }
}

export default Login;