import React from "react"
import ReactDOM from "react-dom"
import Router from "./pages/Index";


export default function App() {
    return (
        <Router />
    );
}

ReactDOM.render(<App />, document.getElementById("root"))
